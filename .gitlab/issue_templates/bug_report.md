# Summary

(Summarize the bug encountered concisely)

# Environment
1. AMIRIS-examples hash: `abc123de` 
2. AMIRIS-version: `x.x.x`
3. FAME-Core version: `x.x.x`
4. Java version: `x.x.x`
5. FAME-Io version: `x.x.x`
6. Python version: `x.x.x`
7. OS: `Win10`

# Expected result:
(What you should see)

# Actual result: 
(What actually happens)

# Additional steps to reproduce
(If any additional steps are required to reproduce the error (besides compiling the input.pb, running AMIRIS and converting outputs)
1. 
2.

# Relevant logs and/or screenshots 
(If possible, please provide some screenshots demonstrating the bug)
(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise.)

# Other
(Feel free to add any information you think is relevant in order to adress this issue)

# Possible fixes
(If you can, link to the line of code that might be responsible for the problem)

/label ~bug