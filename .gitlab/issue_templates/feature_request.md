# Request Summary
(Summarize your idea or the feature you request)

# Use case
(Explain how this would add to the AMIRIS-examples project)

# Images / Sketches
(If possible, please provide some screenshots or sketches to describe your idea even better)

# Other
(Feel free to add any information you think is relevant in order to address this request)

/label ~feature-request