# Austria 2019 Example
Historical simulation of Austrian day-ahead market in 2019

## Time Series
| File                          | Description                                                                                              | Unit        |
|-------------------------------|----------------------------------------------------------------------------------------------------------|-------------|
| `biomass_profile.csv`         | Normalized profile of biomass generation                                                                 | 1           |
| `co2_price.csv`               | Price of CO2 emission allowances                                                                         | EUR/t_CO2   |
| `hydro_reservoir_profile.csv` | Normalized profile of generation for hydro-reservoir and pumped hydro storage operating "reservoir-like" | 1           |
| `load.csv`                    | Hourly electricity demand to be served                                                                   | MWh/h       |
| `natural_gas_price.csv`       | Delivery price to power plant per thermal MWH of natural gas                                             | EUR/MWh_th  |
| `run_of_river_profile.csv`    | Normalized profile of generation for run-of-river power plants                                           | 1           |
| `solar_profile.csv`           | Normalized profile of generation for PV power plants                                                     | 1           |
| `wind_onshore_profile.csv`    | Normalized profile of generation for on-shore wind power plants                                          | 1           |
