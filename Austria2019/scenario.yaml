# AMIRIS back-testing scenario for Austrian day-ahead electricity market in 2019
# Latest version at: https://gitlab.com/dlr-ve/esy/amiris/examples/
# Built for AMIRIS version: see project README at: ../README.md

Schema: !include "schema.yaml"
Variables:
  - &portfolioBuildingOffset 60

GeneralProperties:
  RunId: 1
  Simulation:
    StartTime: 2018-12-31_23:58:00
    StopTime: 2019-12-31_23:58:00
    RandomSeed: 1
  Output:
    Interval: 100
    Process: 0

Agents:
  - Type: DayAheadMarketSingleZone
    Id: 1
    Attributes:
      Clearing: &clearingParameters
        DistributionMethod: SAME_SHARES
      GateClosureInfoOffsetInSeconds: 11

  - Type: CarbonMarket
    Id: 3
    Attributes:
      OperationMode: FIXED
      Co2Prices: "./timeseries/co2_price.csv"

  - Type: FuelsMarket
    Id: 4
    Attributes:
      FuelPrices:
        - FuelType: OIL
          Price: 65.0
          ConversionFactor: 0.6141751628 #conversion from Brent oil price to MWt (megawatts thermal)
        - FuelType: HARD_COAL
          Price: 5.0
          ConversionFactor: 1.0
        - FuelType: NATURAL_GAS
          Price: "./timeseries/natural_gas_price.csv"
          ConversionFactor: 1.0

  - Type: DemandTrader
    Id: 100
    Attributes:
      Loads:
        - ValueOfLostLoad: 3000.0
          DemandSeries: "./timeseries/load.csv"

  - Type: MeritOrderForecaster
    Id: 6
    Attributes:
      Clearing: *clearingParameters
      ForecastPeriodInHours: 168
      ForecastRequestOffsetInSeconds: 27

  - Type: StorageTrader #Pumped Hydro
    Id: 7
    Attributes:
      ElectricityForecastRequestOffsetInSeconds: 22
      Device:
        EnergyToPowerRatio: 19.5
        SelfDischargeRatePerHour: 0.0
        ChargingEfficiency: 0.90
        DischargingEfficiency: 0.90
        InitialEnergyLevelInMWH: 15000
        InstalledPowerInMW: 3400.0
      Strategy:
        StrategistType: SINGLE_AGENT_MIN_SYSTEM_COST
        ForecastPeriodInHours: 168
        ScheduleDurationInHours: 24
        SingleAgent:
          ModelledChargingSteps: 50

  - Type: NoSupportTrader
    Id: 12
    Attributes:
      ShareOfRevenues: 0.0

  - Type: SystemOperatorTrader
    Id: 13

  - Type: SupportPolicy
    Id: 90
    Attributes:
      SetSupportData:
        - Set: PVRooftop
          FIT:
            TsFit: 76.7
        - Set: WindOn
          FIT:
            TsFit: 81.2

  - Type: ConventionalPlantOperator
    Id: 501

  - Type: PredefinedPlantBuilder
    Id: 2001
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: HARD_COAL
        SpecificCo2EmissionsInTperMWH: 0.354
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.98
        OpexVarInEURperMWH: 2.0
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.40
        Maximal: 0.40
      BlockSizeInMW: 50.0
      InstalledPowerInMW: 264.0

  - Type: ConventionalTrader
    Id: 1001
    Attributes:
      minMarkup: -15.0
      maxMarkup: 0.0

  - Type: ConventionalPlantOperator
    Id: 503

  - Type: PredefinedPlantBuilder
    Id: 2003
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: NATURAL_GAS
        SpecificCo2EmissionsInTperMWH: 0.201
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.97
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0
      Efficiency:
        Minimal: 0.40
        Maximal: 0.60
      BlockSizeInMW: 200.0
      InstalledPowerInMW: 3260.0

  - Type: ConventionalTrader
    Id: 1003
    Attributes:
      minMarkup: -5.0
      maxMarkup: 15.0

  - Type: ConventionalPlantOperator
    Id: 504

  - Type: PredefinedPlantBuilder
    Id: 2004
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: NATURAL_GAS
        SpecificCo2EmissionsInTperMWH: 0.201
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.97
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.30
        Maximal: 0.40
      BlockSizeInMW: 100.0
      InstalledPowerInMW: 1208

  - Type: ConventionalTrader
    Id: 1004
    Attributes:
      minMarkup: 0.0
      maxMarkup: 120.0

  - Type: ConventionalPlantOperator
    Id: 505

  - Type: PredefinedPlantBuilder
    Id: 2005
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: OIL
        SpecificCo2EmissionsInTperMWH: 0.264
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.93
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.35
        Maximal: 0.35
      BlockSizeInMW: 100.0
      InstalledPowerInMW: 178

  - Type: ConventionalTrader
    Id: 1005
    Attributes:
      minMarkup: 0.0
      maxMarkup: 120.0

  - Type: VariableRenewableOperator
    Id: 10
    Attributes:
      Set: PVRooftop
      EnergyCarrier: PV
      SupportInstrument: FIT
      InstalledPowerInMW: 1193
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 20
    Attributes:
      Set: WindOn
      SupportInstrument: FIT
      EnergyCarrier: WindOn
      InstalledPowerInMW: 3035
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 50
    Attributes:
      EnergyCarrier: RunOfRiver
      InstalledPowerInMW: 7998
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/run_of_river_profile.csv"

  - Type: VariableRenewableOperator
    Id: 51
    Attributes:
      EnergyCarrier: RunOfRiver #reservoir + pumped hydro storage operating "reservoir-like"
      InstalledPowerInMW: 2000 #estimate
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/hydro_reservoir_profile.csv" #profile can be >1: profile represents reservoir + pumped hydro storage operating "reservoir-like"

  - Type: VariableRenewableOperator
    Id: 52
    Attributes:
      EnergyCarrier: Other #biomass
      InstalledPowerInMW: 500
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/biomass_profile.csv"

  - Type: VariableRenewableOperator
    Id: 53
    Attributes:
      EnergyCarrier: Other #waste
      InstalledPowerInMW: 122
      OpexVarInEURperMWH: 1.2
      YieldProfile: 1

Contracts: !include ["contracts/*.yaml", "Contracts"]
