Contents of files in this folder were derived from data available at external sources, namely

1. [SMARD Strommarktdaten](https://www.smard.de)
2. [E-CONTROL](https://www.e-control.at/statistik/g-statistik/archiv/marktstatistik/preisentwicklungen)
3. [APG](https://www.apg.at/en/markt)
4. [EEX](https://www.eex.com)

Please consider the source and licenses for the content of each file:

| File                          | Source                                                                                                | License                                                                                                                                               |
|-------------------------------|-------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `biomass_profile.csv`         | [SMARD](https://www.smard.de)                                                                         | CC BY 4.0                                                                                                                                             |
| `co2_price.csv`               | [EEX](https://www.eex.com)                                                                            | No license. EEX kindly granted us permission to use and publish their data in this repository for scientific purposes.                                |
| `hydro_reservoir_profile.csv` | Own calculation based on [APG](https://www.apg.at/en/markt)                                           | No licence. APG kindly granted us permission to use and publish their data in this repository for scientific purposes. Please see the APG disclaimer. |
| `load.csv`                    | [SMARD](https://www.smard.de)                                                                         | CC BY 4.0                                                                                                                                             |
| `natural_gas_price.csv`       | [E-CONTROL](https://www.e-control.at/statistik/g-statistik/archiv/marktstatistik/preisentwicklungen)  | No licence                                                                                                                                            |
| `run_of_river_profile.csv`    | [APG](https://www.apg.at/en/markt)                                                                    | No licence. APG kindly granted us permission to use and publish their data in this repository for scientific purposes. Please see the APG disclaimer. |
| `solar_profile.csv`           | [SMARD](https://www.smard.de)                                                                         | CC BY 4.0                                                                                                                                             |
| `wind_onshore_profile.csv`    | [SMARD](https://www.smard.de)                                                                         | CC BY 4.0                                                                                                                                             |

*APG Disclaimer:  
APG is not liable for incorrect or missing information. Therefore, all decisions based on information from the APG website are the sole responsibility of the user. In particular, APG shall not be liable for any direct, specific or consequential damage or other damage of any kind whatsoever arising in connection with the indirect or direct use of the information provided on the APG websites.*
