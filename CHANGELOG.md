<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <amiris@dlr.de>

SPDX-License-Identifier: CC0-1.0 -->

# Changelog
## [2.0.1](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v2.0.1) - 2024-03-14
### Fixed
- Germany 2016: fixed wrong datapoints in RES timeseries #37 (@dlr_fn, @dlr-cjs)


## [2.0.0](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v2.0.0) - 2024-03-12
_If you are upgrading: please see `UPGRADING.md`_
### Changed
- **Breaking**: Update schema.yaml to conform with latest AMIRIS version #26, #28, #29, #31, #34 (@dlr-cjs, @dlr_jk)
- **Breaking**: Update scenarios to conform with latest AMIRIS version #26, #29, #31 (@dlr-cjs, @dlr_jk)
- **Breaking**: Harmonized timeseries names to snake_case #12 (@dlr_fn)
- Germany 2019: Update all timeseries and markups based on automated data workflow #24 (@dlr-cjs)

### Added 
- Added `Germany2015` example (#19 @dlr-cjs @dlr_fn)
- Added `Germany2016` example (#20 @dlr-cjs @dlr_fn)
- Added `Germany2017` example (#21 @dlr-cjs @dlr_fn)
- Added `Germany2018` example (#22 @dlr-cjs @dlr_fn)
- Germany 2019: Added power plant availability time series for `hard_coal`, `lignite`, and `nuclear` #12 (@dlr-cjs)
- Added missing policy contracts in scenarios `Austria2019` and `Simple` #32 (@dlr_jk, @litotes18, @dlr-cjs)

### Fixed
- Germany 2019: Remove drops from demand timeseries caused by missing data #24 (@dlr-cjs) 
- All scenarios: Fixed racing condition for Payout action in ConventionalPlantOperator contracts #33 (@dlr_jk, @litotes18, @dlr-cjs)

## [1.1](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v1.1) - 2023-10-19
### Changed
- Updated schema.yaml to conform with latest AMIRIS version #23 (@dlr-cjs)

### Added
- `README.md` to each scenario with units and description of timeseries

### Fixed
- Fuel price timeseries in `Germany2019` example are now calculated correctly #17 (@dlr_fn, @dlr-cjs)

## [1.0](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v1.0) - 2022-08-30
_Initial release_
