[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7789049.svg)](https://doi.org/10.5281/zenodo.7789049)

# Examples
Example configurations built for [AMIRIS 2.0.0](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v2.0.0)
See the individual licence information in the respective scenarios.

## How-to
Follow these steps to start using AMIRIS:

1. Clone [AMIRIS](https://gitlab.com/dlr-ve/esy/amiris/amiris). 
2. Clone this project to the `input` folder in your AMIRIS project repository.
3. [Compile the binary input](https://gitlab.com/fame-framework/fame-io#make-a-fame-run-configuration) file by using [FAME-Io](https://pypi.org/project/fameio) (`makeFameRunConfig -f <./path/to/scenario.yaml>`). Alternatively, you may download pre-compiled `.pb` input files [here](https://gitlab.com/dlr-ve/esy/amiris/examples/-/jobs/artifacts/main/download?job=packaging).
4. Execute AMIRIS.
5. [Convert the binary results file](https://gitlab.com/fame-framework/fame-io#read-fame-results) by using [FAME-Io](https://pypi.org/project/fameio) (`convertFameResults -f <./path/to/protobuf_file.pb>`).

## Sample scenarios
* [Simple](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Simple): simple scenario running for one day with dummy data
* [Austria2019](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Austria2019): historical simulation of Austrian day-ahead market in 2019
* [Germany2015](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2015): historical simulation of German day-ahead market in 2015
* [Germany2016](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2016): historical simulation of German day-ahead market in 2016
* [Germany2017](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2017): historical simulation of German day-ahead market in 2017
* [Germany2018](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2018): historical simulation of German day-ahead market in 2018
* [Germany2019](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2019): historical simulation of German day-ahead market in 2019

## Citing AMIRIS Examples
If you use AMIRIS Examples in your scientific work please cite:

Kristina Nienhaus, Christoph Schimeczek, Ulrich Frey, Evelyn Sperber, Seyedfarzad Sarfarazi, Felix Nitsch, Johannes Kochems & A. Achraf El Ghazi (2023). AMIRIS Examples. Zenodo. [doi: 10.5281/zenodo.7789049](https://doi.org/10.5281/zenodo.7789049)

## Acknowledgments
We thank those who contributed to enhancing the AMIRIS examples:
* [v1.1](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v1.1) Dr. Tom Bauermann, for pointing out errors in historical fuel prices